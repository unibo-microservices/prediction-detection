# Temperature Sensor


The project was made through internship erasmus program. The main goal was to develop an ML timeseries inference. More deeply, this application was developed to make a prediction of a machine's temperature for predictive maintenance. It is composed of three microservices: 

> ms1, which is a representation of a sensor which sends the temperature data

> ms2 : which makes a prediction and finally

> ms3: which checks the prediction and arises an ALARM if needed




### ms1

This microservice represents a sensor. It reads the csv data and sends them line by line to ms2 every 1 second. The csv file was splitted into 2 parts. One of them was used for the offline training of the model(for_model.csv) and the other one for representing the sensor's new sending values(temperature.csv).


### ms2

The ms2 is responsible for receiving the sensor's data and call an already trained model (SARIMA)to make a prediction. To achieve this it stores each ms1 data received to a history file wich reads and makes a prediction.This helps to use for the next predicted value all the history data + the new incoming value. The predictions are stored into a table which's length is everytime increased and they are send line by line to the ms3.


### ms3

Ms3 receives line by line the predicted values and raises an ALARM meggase if the value is higher than 130 degrees.


**The csv file was found on  :**

*https://github.com/limingwu8/Predictive-Maintenance/blob/master/dataset/csv/original/1711_TANK_TEMPERATURE.csv* 




