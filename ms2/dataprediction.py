from flask import Flask,request
import pandas as pd
import joblib
import requests
from sklearn.model_selection import train_test_split
import pandas as pd
import time
from pandas import read_csv
import re
import datetime

app = Flask(__name__)

@app.route('/dataprediction', methods=['GET','POST'])


def model_prediction():

    start = datetime.datetime.now()
    start_ms2_3 = datetime.datetime.now()
    r = request.json
    rstr = str(r["data"])

    # after getting the response remove all unwanted characters and keep only numeric values, - , :, spaces and .
    result = re.sub(r'[^0-9,^"\-",^":"^,^" ", ^"."]', '', rstr)

    # write each new income line to a file as a history
    with open('history.csv', 'a') as f:
        f.write(f"{result}\n")
    dfs = read_csv('history.csv',names =["Date","sensor_temp_cont"], sep=",")
    resdfs = pd.DataFrame(dfs)
    resdfs['Date'] = pd.to_datetime(resdfs['Date']).dt.date
    print(resdfs)

    # call the model
    model = joblib.load('SARIMAmodel_fit.pkl')
    y_pred = model.get_forecast(len(resdfs.index))
    y_pred_df = y_pred.conf_int()
    y_pred_df = y_pred.conf_int()

    # save predictions
    y_pred_df["Predictions"] = model.predict(start = y_pred_df.index[0], end = y_pred_df.index[-1], dynamic=True)
    y_pred_df.index = resdfs.index
    y_pred_out = y_pred_df["Predictions"]
    stop = datetime.datetime.now()
    execution = stop - start

    # send each prediction line by line to ms3 for checking
    for i in range (0,len(y_pred_out)):
        # send = requests.post('http://127.0.0.1:5005/postingresults', json={'data': y_pred_out[i] })
        send = requests.post('http://172.17.0.4:5005/postingresults', json={'data': y_pred_out[i] })
        time.sleep(1)
    print(send)
    stop_ms2_3 = datetime.datetime.now()
    execution_ms2_3 = stop - start
    print("MS2 & MS3 -- Total process in microseconds: ", execution_ms2_3.microseconds)
    return r

 

    
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5004)
