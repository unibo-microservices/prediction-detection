import pandas as pd
import pickle
from sklearn.datasets import load_diabetes
from sklearn.model_selection import train_test_split
from sklearn.model_selection import train_test_split
import statsmodels.api as sm
from pandas import read_csv

dfs = read_csv('for_model.csv',names =["date_time","sensor_temp_cont"], sep=",")
dfs['Date'] = pd.to_datetime(dfs['date_time']).dt.date
dfs['Time'] = pd.to_datetime(dfs['date_time']).dt.time
dfl = dfs[['Date', 'Time', 'sensor_temp_cont']]
df = dfl.groupby('Date')['sensor_temp_cont'].mean().reset_index()
train, test = train_test_split(df,test_size=0.2, shuffle=False)
y = train['sensor_temp_cont']
SARIMAmodel = sm.tsa.statespace.SARIMAX(y, order=(2,0,0), seasonal_order=(1,0,1,6))
SARIMAmodel = SARIMAmodel.fit()
y_pred = SARIMAmodel.get_forecast(len(test.index))
y_pred_df = y_pred.conf_int() 

pickle.dump(SARIMAmodel, open('SARIMAmodel_fit.pkl', 'wb'))