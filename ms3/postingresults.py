from flask import Flask,request
import datetime

app = Flask(__name__)

@app.route('/postingresults', methods=['GET','POST'])
def take_decision():

   start = datetime.datetime.now()
   response = request.json

   # check the value and arises an alarm if the predicted temperature value is higher than 130
   if(response["data"] > 130.0):
      print("ALARM TEMP HIGHER THAN 130")
   print(response["data"])

   stop = datetime.datetime.now()
   execution = stop - start
   print("MS3: Temperature value check & Alarm message set in microseconds: ", execution.microseconds)

   return str(response["data"])



if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5005)