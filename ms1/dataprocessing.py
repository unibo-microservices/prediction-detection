from flask import Flask
import requests
import time
import json
import datetime
# dataset source : https://github.com/limingwu8/Predictive-Maintenance/blob/master/dataset/csv/original/1711_TANK_TEMPERATURE.csv
app = Flask(__name__)

@app.route('/dataprocessing', methods=['GET','POST'])
def ReadSensorData():
    start = datetime.datetime.now()
    # opens the csv and reads it line by line
    with open("temperature.csv", "r") as fp:
        line = fp.readline().splitlines()
        line = fp.readline()
        while line:
            # sending each line it reads to ms2
            # nondocker
            # r = requests.post('http://127.0.0.1:5004/dataprediction', json={'data': line})
            # docker
            r = requests.post('http://172.17.0.3:5004/dataprediction', json={'data': line})
            line = fp.readline().splitlines() 
            print(line)
            # sleep for 1s
            time.sleep(1)
            stop = datetime.datetime.now()
            execution = stop - start
            ex_time = execution.total_seconds() * 1000
            print("MS1+MS2: Total process in ms: ", ex_time)
    return json.dumps(r.json())

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5003)